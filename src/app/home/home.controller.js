
angular
.module('app')
.controller('HomeController', ['$scope', '$window', 'iTunesService', '_',
function($scope, $window, iTunesService, _ ) {
    
    $scope.topAlbums = [];
    $scope.showFavouriteAlbums = false;
    $scope.searchText = '';

    var Init = function () {

        iTunesService.topAlbums()
        .then(function(response) {

            var albums = response.data.feed.entry;
            albums.forEach( function(album) {
              
                var id = _.get(album, 'id.attributes.im:id', '')
                iTunesService.albumSongs(id)
                .then( function(response) {
                 
                    if (response.data.resultCount >= 2) 
                        var previewUrl = response.data.results[1].previewUrl
                      
                    var images =  _.get(album, 'im:image');
                    var image = '';
                    if (images.length)
                        image = images[images.length-1].label
                
                    $scope.topAlbums.push({
                        id: _.get(album, 'id.attributes.im:id', ''),
                        title : _.get(album, 'title.label', ''),
                        image: image,
                        price :  _.get(album, 'im:price.label', '0'),
                        releaseDate : _.get(album, 'im:releaseDate.label', ''),
                        link: _.get(album, 'link.attributes.href', ''),
                        previewUrl: previewUrl || '',
                        favourite: false
                    });
                    
                },function(error){

                    console.log('Something went wrong');
                });
                
            });
        
            console.log($scope.topAlbums);
        
        }, function(error){

            console.log('Something went wrong');
        });
    }

    $scope.albumsDetail = function(link) {
        if (link)
            $window.open(link, '_blank');
    }

    $scope.addTofavourite = function(album) {
        album.favourite =  !album.favourite;
    }
       
    
    Init();

}]);
