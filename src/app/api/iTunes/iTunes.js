
angular
.module('app')
.factory('iTunesService', ['$http', '$sce', function($http, $sce) {
  

    var methods = {};
    
    methods.topAlbums = function(place) {

        return $http({
            url: 'https://itunes.apple.com/us/rss/topalbums/limit=100/json', 
            method: "GET"
        })
        
    }

    methods.albumSongs = function(id) {
           
        var url = "https://itunes.apple.com/lookup"
        url = $sce.trustAsResourceUrl(url);

        return $http.jsonp(url, 
        {  
            params: {
                'jsonpCallbackParam': 'callback',
                'id' : id,
                'entity' : 'song'
            }
        })
       
       
    }
   
    return methods;

}]);

